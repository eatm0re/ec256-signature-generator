package edu.eatmore.ec256generator;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.EllipticCurveProvider;

import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

public class Runner {

  public static void main(String[] args) {
    System.out.println("\n\nGenerating key pair...");
    KeyPair keyPair = EllipticCurveProvider.generateKeyPair(SignatureAlgorithm.ES256);
    PrivateKey privateKey = keyPair.getPrivate();
    PublicKey publicKey = keyPair.getPublic();
    System.out.println("\nGenerated private key:");
    System.out.println(privateKey);
    System.out.println("Generated public key:");
    System.out.println(publicKey);

    System.out.println("\nEncoding keys...");
    byte[] privateEncoded = privateKey.getEncoded();
    System.out.println("Encoded private key. Length: " + privateEncoded.length);
    byte[] publicEncoded = publicKey.getEncoded();
    System.out.println("Encoded public key. Length: " + publicEncoded.length);

    System.out.println("\nConverting keys to Base64...");
    String privateBase64 = new String(Base64.getEncoder().encode(privateEncoded));
    System.out.println("\nBase64 private key:");
    System.out.println(privateBase64);
    String publicBase64 = new String(Base64.getEncoder().encode(publicEncoded));
    System.out.println("\nBase64 public key:");
    System.out.println(publicBase64);

    System.out.println("\n\nWriting keys to files...");
    try (FileWriter privateKeyFileWriter = new FileWriter("private.key");
         FileWriter publicKeyFileWriter = new FileWriter("public.key")) {
      privateKeyFileWriter.write(privateBase64);
      publicKeyFileWriter.write(publicBase64);
      System.out.println("... wrote.\n\n");
    } catch (IOException e) {
      System.out.println("Could not write keys to files!");
      System.out.println(e);
    }
  }
}
